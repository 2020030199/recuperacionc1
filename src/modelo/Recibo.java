
package modelo;

public class Recibo {
private int numRecibo;
    private String fecha;
    private String nombre;
    private String domicilio;
    private int tipoServicio;
    private double costoKW;
    private int kwConsumidos;
    
    public Recibo() {
        // Constructor por defecto
    }
    
    public Recibo(int numRecibo, String fecha, String nombre, String domicilio, int tipoServicio, double costoKW, int kwConsumidos) {
        this.numRecibo = numRecibo;
        this.fecha = fecha;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.tipoServicio = tipoServicio;
        this.costoKW = costoKW;
        this.kwConsumidos = kwConsumidos;
    }
    
    public void calcularPago() {
        double subTotal = costoKW * kwConsumidos;
        double impuesto = subTotal * 0.16;
        double totalPagar = subTotal + impuesto;

        System.out.println("Num. Recibo: " + numRecibo);
        System.out.println("Fecha: " + fecha);
        System.out.println("Nombre: " + nombre);
        System.out.println("Domicilio: " + domicilio);
        System.out.println("Tipo Servicio: " + tipoServicio);
        System.out.println("Costo por Kilowatts: " + costoKW);
        System.out.println("Kilowatts Consumidos: " + kwConsumidos);
        System.out.println("Sub Total: " + subTotal);
        System.out.println("Impuesto (16%): " + impuesto);
        System.out.println("Total a Pagar: " + totalPagar);
    }
    
    public void recursamiento() {
        // Implementación del método recursamiento
    }
    
    // Getters y setters
    // ...
}    

